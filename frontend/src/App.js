import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import './App.scss';
import Page from './pages/Page'
import Login from './pages/Login'
import Main from './pages/Main'
import ArticleView from './pages/ArticleView'

import Writing from './pages/Writing'
import None from './pages/None'

function App() {
  return (
    <div className='app'>
      <Router >
        <Switch>
          {/* /绝对主页 ，放置留言板和头像登录入口，*/}
          <Route path='/' exact component={Page} />
          {/* 登录页 */}
          <Route path='/login' component={Login} />
          {/* 个人主页 */}
          <Route path='/main' component={Main} />
          {/* 文章浏览页 */}
          <Route path='/articleView' component={ArticleView} />
          {/* 文章编辑页*/}
          <Route path='/writing' component={Writing} />
          {/* 404页 */}
          <Route path='/none' component={None} />
        </Switch>
        {/* <Redirect  to='/none'/> */}
      </Router>
    </div>
  );
}

export default App